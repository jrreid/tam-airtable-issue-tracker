// Load environment variables from .env file in project directory
require('dotenv').config();

// Load modules
const url = require('url');
const { Gitlab } = require('@gitbeaker/node');
const Airtable = require('airtable');

const openIssuesBaseName = process.env.AIRTABLE_OPEN_ISSUES_BASE;
const closedIssuesBaseName = process.env.AIRTABLE_CLOSED_ISSUES_BASE;

const deleteOpenIssuesUponClose = true;

// Test for presence of required env variables, set a sane default if not provided
if(!process.env.AIRTABLE_API_URL) {
  process.env.AIRTABLE_API_URL = 'https://api.airtable.com'
}

// Construct a new airtable configuration object based on environment variables
// TODO: add some error statements if these environment vars are not present
var base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY,
  endpointUrl: process.env.AIRTABLE_API_URL
}).base(process.env.AIRTABLE_BASE);

// Construct a new gitlab configuration object based on environment variables
// TODO: add some error statements if these environment vars are not present
const gitlab = new Gitlab({
  host: process.env.GITLAB_HOST,
  token: process.env.GITLAB_TOKEN
});

base(openIssuesBaseName).select({
  maxRecords: 200,
  view: "Grid view"
}).eachPage(function page(records, fetchNextPage) {
  // This function (`page`) will get called for each page of records.

  records.forEach(function(record) {

    // Parse out URL path from the URL eg. https://website.com/1/2/3, urlPath == '/1/2/3'
    const urlPath = url.parse(record.get('URL')).pathname;
    //console.log(urlPath);

    const gitlabProject = parseProjectPath(urlPath);
    const issueId = parseIssueId(urlPath);

    var changelog = [];

    gitlab.Issues.show(gitlabProject, issueId).then((issue) => {
      console.log('Project:', gitlabProject)
      console.log('--> issueId:', issueId)
      console.log('----> title:',issue.title);
      console.log('------------------------------------');

      // Updates Airtable Issue title to match the gitlab issue's title in the event of a disagreement
      // Logs the change in title to the changelog.
      var airtableIssueTitle = record.get('title');
      if(airtableIssueTitle != issue.title && airtableIssueTitle != undefined) {
        // Update the title of the issue's record in Airtable
        record.patchUpdate({
          'title': issue.title
        }).catch(function logErr(err) {
          console.log(err);
        });

        // Update the issue's changelog in Airtable to reflect the change in title
        changelog.push('Title updated. Previous title: \"' + airtableIssueTitle + '\"');
      };

      // Updates Airtable Issue milestone to match the gitlab issue's milestone in the event of a disagreement
      // Logs the change in milestone to the changelog.
      var airtableIssueMilestone = record.get('milestone');
      if(issue.milestone && (airtableIssueMilestone != issue.milestone.title)) {
        record.patchUpdate({
          'milestone': issue.milestone.title
        }).catch(function logErr(err) {
          console.log(err);
        });

        // Update the issue's changelog in Airtable to reflect the change in Milestone
        changelog.push('Milestone updated. Previous milestone: \"' + airtableIssueMilestone + '\"');
      };

      // Update Airtable with a dage_logged if there isn't a value present.
      if(!record.get('date_logged')) {
        record.patchUpdate({
          'date_logged': getIsoDate()
        })
        // Update the issue's changelog in Airtable to reflect the addition of a `date_logged`
        changelog.push('No date_logged. Recorded as logged today.');
      }

      // If Issue state is not "opened", mark as closed in the changelog.
      if(issue.state != 'opened') {
        changelog.push('Issue closed.');
      }

      // If there's changelog events, build a complete string and log it in airtable
      if(changelog.length > 0) {

        // Build a changelog string based on all elements of the changelog array
        var changelogString = buildChangeLogString(changelog);

        // append the airtable record's preexisting changelog to the new string
        var airtableChangelog = record.get('Changelog');
        if(airtableChangelog) {
          changelogString += airtableChangelog;
        }

        // Update the changelog of the issue's record in Airtable
        record.patchUpdate({
          'Changelog': changelogString
          // once the changelog has been updated within the openIssues record and the promise fulfilled, copy (and optionally delete) the openIssues record to closedIssues
        }).then(function (updatedRecord) {
          if(issue.state !='opened') {
            copyClosedIssue(updatedRecord, issue);
          }
        }).catch(function logErr(err) {
          console.log(err);
        });
      }
    }).catch(function logError(err) {
      console.log(err);
    });
  });

  // To fetch the next page of records, call `fetchNextPage`.
  // If there are more records, `page` will get called again.
  // If there are no more records, `done` will get called.
  fetchNextPage();

}, function done(err) {
  if (err) { console.error(err); return; }
});

/////////////////////////////////////////////////////////////////////////

// Accepts an array of changes `changelog` and returns a string with current ISO date, each change text and newline separator
function buildChangeLogString(changelog) {
  var changelogString = new String();
  //Build changelogString
  changelog.forEach(change => {
    changelogString += getIsoDate() + ' - ' + change + '\r\n';
  });

  return changelogString;
}


// This takes a complete airtable `record` object and gitlab issue as arguments, fetches the full content of the record,
// adds the gitlab issue's `closed_at` value to the records fields
// creates a copy of the record in closedIssues
// optionally deletes the openIssues record
function copyClosedIssue(airtableRecord, gitlabIssue) {
  airtableRecord.fetch().then(function (fullRecord) {

    // Added Gitlab Issue's closed_at date to the issue -- takes everything left of the T in "2020-05-11T17:33:12.336Z" for example
    fullRecord.fields.date_closed = String(gitlabIssue.closed_at).split('T')[0]

    base(closedIssuesBaseName).create(fullRecord.fields).then(function(){
      if(deleteOpenIssuesUponClose) {
        base(openIssuesBaseName).destroy(airtableRecord.id).catch(function logError(err) {
          console.log(err);
        });
      }
    }).catch(function logError(err) {
      console.log(err);
    });
  }).catch(function logError(err) {
    console.log(err);
  });
}

// Accepts a url path string as an argument, returns gitlab project path
function parseProjectPath(urlPathString) {
  // extracts GitLab project path with the following regex:
  // ^\/ matches a '/' at the beginning of the string
  // (.*?) lazy capture group to grab all characters
  // \/-|\/issues|\/epics capture group completed by either a '/-' or '/issues' OR '/epics'
  const projectPath = urlPathString.match(/^\/(.*?)(\/-|\/issues|\/epics)/)[1];
  return encodeURI(projectPath);
}

// Accepts a url path string as an argument, returns gitlab issue id
// **TODO 2020-05-12**: detect if epic, handle appropriately, prob not here.
function parseIssueId(urlPathString) {
  // extracts GitLab issue Id with the following regex:
  // https://stackoverflow.com/posts/19777066/revisions
  const issueId = parseInt(urlPathString.match(/\/([^\/]+)\/?$/)[1]);
  return issueId;
}

// https://stackoverflow.com/questions/25159330/convert-an-iso-date-to-the-date-format-yyyy-mm-dd-in-javascript
function getIsoDate() {
  var today = new Date();
  return today.toISOString().substring(0,10);
}